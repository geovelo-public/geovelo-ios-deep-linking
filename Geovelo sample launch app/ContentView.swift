//
//  ContentView.swift
//  Geovelo sample launch app
//
//  Created by Cedric Gatay on 16/03/2020.
//  Copyright © 2020 La compagnie des mobilités. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Button(action: {
                DestinationPoint(latitude: 47.4018684,
                                 longitude: 0.6872403,
                                 name: nil)
                    .launch()
            }){
                Text("Address : coordinates")
                    .padding(.vertical)
            }
            Button(action: {
                DestinationPoint(latitude: 47.4018684,
                                 longitude: 0.6872403,
                                 name: "Some address, 37000 Tours, France")
                    .launch()
            }){
                Text("Address : coordinates with title")
                    .padding(.vertical)
            }
            Button(action: {
                DestinationPoint(latitude: 47.4018684,
                                 longitude: 0.6872403,
                                 name: "My address")
                    .launch()
            }){
                Text("Address : 1 location")
                    .padding(.vertical)
            }
            Button(action: {
                [
                    DestinationPoint(latitude: 47.4018684,
                                 longitude: 0.6872403,
                                 name: "Departure address"),
                    DestinationPoint(latitude: 47.4118684,
                                 longitude: 0.6872403,
                                 name: "Arrival address"),
                ]
                    .launch()
            }){
                Text("Address : 2 locations")
                    .padding(.vertical)
            }
            
            Button(action: ContentView.launchAppStore){
                Text("Launch AppStore on GeoVelo page")
                        .padding(.vertical)
            }
        }
    }
   
    
    
    fileprivate static func launchAppStore(){
       let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id1261357713"
       UIApplication.shared.open(URL(string: urlStr)!)
    }
    
}


struct DestinationPoint{
    let latitude: Double
    let longitude: Double
    let name: String?
    
    var uriComponent: String{
        let base = "\(latitude),\(longitude)"
        if let name = name?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            return "\(base),\(name)"
        }
        return base
    }
}


extension DestinationPoint{
   fileprivate static func buildURL(_ points: [DestinationPoint]) -> URL?{
       let base = "geovelo://itineraries?"
       if points.count < 2, let point = points.first{
           return URL(string: "\(base)location=\(point.uriComponent)")
       }
       let params = points.map { "location[]=\($0.uriComponent)" }.joined(separator: "&")
       return URL(string: "\(base)\(params)")
   }
   
   fileprivate static func launchGeovelo(_ url: URL?){
       guard let url = url else { return }
       UIApplication.shared.open(url, options: [:]){opened in
           //GV is not available, open website instead
           if !opened{
               UIApplication.shared.open(URL(string: "https://app.geovelo.fr")!, options: [:])
           }
       }
   }
    
    func launch(){
        [self].launch()
    }
}

extension Collection where Element == DestinationPoint{
    func launch(){
        DestinationPoint.launchGeovelo(DestinationPoint.buildURL(self.map { $0 }))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
